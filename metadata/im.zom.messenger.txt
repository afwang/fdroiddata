Categories:Internet
License:Apache2
Web Site:https://zom.im/
Source Code:https://github.com/zom/zom-android
Issue Tracker:https://github.com/zom/zom-android/issues
Changelog:https://github.com/zom/Zom-Android/blob/HEAD/CHANGELOG

Auto Name:Zom
Summary:Text with friends
Description:
Mobile messenger with focus on ease-of-use and security.
.

Repo Type:git
Repo:https://github.com/zom/zom-android

Build:15.0.0-RC-2,1500206
    disable=builds, wait for reproducible
    commit=15.0.0-RC-2
    subdir=app
    init=echo 'task wrapper(type: Wrapper) { gradleVersion = "2.11" }' >> ../build.gradle
    gradle=zomrelease

Auto Update Mode:None
Update Check Mode:Tags
Current Version:15.0.0-RC-2
Current Version Code:1500206
